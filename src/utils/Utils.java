package utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
	/**
	 * Vérifie si <strong>type</strong> est un nombre
	 * @param type
	 * @return
	 */
	public static boolean isNumber(String type) {
		return (type.compareTo("int") == 0 || type.compareTo("float") == 0 || type.compareTo("double") == 0);
	}
	
	/**
     * Capitalise la première lettre d'une chaine de charactère
     * @param chaine
     * @return
     */
    public static String upperFirst(String chaine) {
        return chaine.substring(0, 1).toUpperCase() + chaine.substring(1);
    }
    
    /**
     * Transforme une date string en Timestamp 
     * @param str
     * @param format
     * @return
     * @throws ParseException
     */
    public static Timestamp strToTimestamp(String str, String format) throws ParseException {
		return new Timestamp(new SimpleDateFormat(format).parse(str).getTime());
    }
    
    public static Timestamp now() {
		return new Timestamp(System.currentTimeMillis());
    }
    
    public static String now(String format) {
    	return new SimpleDateFormat(format).format(new Timestamp(System.currentTimeMillis()));
    }
    
    /**
     * Recuperer la date dans une chaine et la transforme en Timestamp
     * @param date
     * @return
     * @throws Exception
     */
    public static Timestamp getDate(String date) throws Exception {
        if (date == null || date.compareTo("") == 0) {
            throw new Exception("Date required !!!");
        }
        Timestamp result = null;
        Pattern pFr = Pattern.compile("[0-9]{1,2}[ .,/:;-][0-9]{1,2}[ .,/:;-][0-9]{4}");
        Pattern pEn = Pattern.compile("[0-9]{4}[ ,./:;-][0-9]{1,2}[ ,./:;-][0-9]{1,2}");
        Pattern pH = Pattern.compile("[0-9]{2}[ .,/:;-][0-9]{2}[ .,/:;-][0-9]{2}");
        /// test le format de la date entrer
        Matcher m = pEn.matcher(date);
        String dat = null, hour = "";
        if (m.find()) {
            dat = m.group();
        } else {
            m = pFr.matcher(date);
            if (m.find()) {
                dat = m.group();
            } else {
                throw new Exception("Date format invalid ,input ->" + date + ". required -> yyyy-mm-dd or dd-mm-yyyy");
            }
        }
        /// enlevement du date trouver dans la chaine
        date = date.replace(dat, "");
        /// test si l'heure est inclu dedans
        m = pH.matcher(date);
        if (m.find()) {
            hour = format(m.group());
        } else {
            hour = "00:00:00";
        }
        /// formater la date
        dat = format(dat);
        try {
            result = Timestamp.valueOf(dat + " " + hour);
        } catch (Exception e) {
            throw new IllegalArgumentException("input date not exists -> " + dat + " " + hour);
        }
        return result;
    }
    
    private static String format(String str) throws Exception {
        String sep = ":";
        String[] list = null;
        try {
            list = str.split("[ .,/:;-]");
            if (list[2].length() == 4) {
                sep = "-";
                String temp = list[2];
                list[2] = list[0];
                list[0] = temp;
            }
            if (list[0].length() == 4) {
                sep = "-";
            }
        } catch (Exception e) {
            throw new Exception("REFORM Exception");
        }
        return list[0] + sep + list[1] + sep + list[2];
    }
}
