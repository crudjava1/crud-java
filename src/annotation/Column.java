package annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation de liaison entre l'attribut de la classe et le nom de la colonne dans la base de donnée  
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Column {
	/**
	 * Nom de la colonne dans la base de donnée
	 * @return
	 */
	String value() default "";
}
