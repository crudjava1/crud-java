package annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * sert de spécification du nom du sequence utilisé par la table dans la base de donnée avec le prefix s'il y en a  
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Seq {
	/**
	 * Nom de la sequence dans la base de donnée
	 * @return
	 */
	String name();
	/**
	 * Prefixe à concatener avant la valeur de la sequence
	 * @return
	 */
	String prefix() default "";
}
