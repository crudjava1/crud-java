package annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Id permet de préciser que l'attribut est une clé primaire dans la table de la base de donnée 
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Id {
}
