package annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Fk permet de spécifier que l'attribut est une clé étrangère en spéficiant le nom de la table à qui appartient la clé étrangère 
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Fk {
	/**
	 * Nom de la table à qui appartient la clé primaire
	 * @return
	 */
	String value();
}
