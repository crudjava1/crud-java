package display;
import java.sql.Timestamp;

import dao.*;
import models.Diplome;
import models.Etudiant;
import utils.Utils;

public class Main {
	public static void main(String[] args) throws Exception {
		try {
//			Dao dao = Dao.getInstance();
//			print(dao.select(new Etudiant(), "select * from etudiant", null));
//			findTest(dao);
//			insertTest(dao);
//			deleteTest(dao);
//			updateTest(dao);
			System.out.println("Hey !");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static void print(Etudiant[] etudiants) {
		if(etudiants == null) return;
		for(Etudiant et : etudiants) {
			System.out.println(et.toString());
		}
	}
	
	static void print(Diplome[] diplomes) {
		if(diplomes == null) return;
		for(Diplome d : diplomes) {
			System.out.println(d.toString());
		}
	}
	
	static void deleteTest(Dao dao) throws Exception {
		/*SqlMaker filtre = null;
		filtre = new SqlMaker("id").like("etu%");
		print(dao.select(new Etudiant(), null));
		dao.delete("etudiant", filtre, null);
		System.out.println("Apres");
		print(dao.select(new Etudiant(), null));*/
		print(dao.select(new Diplome(), null));
		dao.delete("diplome", null);
		System.out.println("\nApres\n");
		print(dao.select(new Diplome(), null));
	}
	
	static void findTest(Dao dao) throws Exception {
		SqlMaker liaison = null;
		liaison = new SqlMaker("id").like("1").and("nom").like("A");
		Etudiant[] etudiants = null;
		etudiants = dao.select(new Etudiant(), 1, null);
		print(etudiants);
	}
	static void updateTest(Dao dao) throws Exception {
		SqlMaker filtre = null;
		filtre = new SqlMaker("id").like("5");
		print(dao.select(new Etudiant(), null));
		Etudiant et = new Etudiant();
		et.setPrenom("Rakotoarivelo");
		dao.update(et, new String[]{"nom"}, Dao.TO_UPDATE, filtre, null);
		System.out.println("\nApres\n");
		print(dao.select(new Etudiant(), null));
	}
	
	static void insertTest(Dao dao) throws Exception {
		Timestamp tstpDate = Utils.strToTimestamp("1980-01-02 00:10:00", "yyyy-MM-dd hh:mm:ss");
		Etudiant etudiant = new Etudiant("Louis", tstpDate , "francais.com");
		Diplome[] diplomes = new Diplome[2];
		diplomes[0] = new Diplome("Bacc");
		diplomes[1] = new Diplome("Licence");
		etudiant.setDiplomes(diplomes);
		System.out.println(dao.insert(etudiant, 1, null, null));
		/*Etudiant[] etudiants = null;
		etudiants = dao.select(new Etudiant(), 0, null);
		print(etudiants);*/
		/*System.out.println("\nApres\n");
		etudiants = dao.select(new Etudiant(), 1, null);
		print(etudiants);*/
	}
}
