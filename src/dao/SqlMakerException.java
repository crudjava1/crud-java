package dao;

@SuppressWarnings("serial")
public class SqlMakerException extends Exception {
	public SqlMakerException(String message) {
		super(message);
	}
}
