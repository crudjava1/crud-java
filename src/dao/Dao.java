package dao;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import annotation.*;
import utils.Utils;

/**
 * Pour utiliser le framework , on doit créer un fichier <strong> dao.properties
 * </strong> dans le répertoire principal du projet<br>
 * Exemple:<br>
 * <ul>
 * <li>Pour Oracle</li> DRIVER_NAME=oracle.jdbc.driver.OracleDriver<br>
 * DB_NAME=oracle<br>
 * USERNAME=test<br>
 * PASSWORD=t<br>
 * JDBC_DATABASE_URL=jdbc:oracle:thin:@localhost:1521:xe<br>
 * <li>Pour postgres</li> DB_NAME=postgres<br>
 * JDBC_DATABASE_URL=jdbc:postgresql://localhost/testdb<br>
 * USERNAME=test<br>
 * PASSWORD=t <br>
 * </ul>
 * 
 * @author Bary Rakotoarivelo
 */
public class Dao {
	public static final int NO_CACHE = 0;
	public static final int SET_CACHE = 1;
	
	public static final int CURRVAL = 20;
	public static final int NEXTVAL = 21;
	
	public static final int IGNORE = 10;
	public static final int TO_UPDATE = 11;
	private static final Dao dao = new Dao();
	
	private Dao() {}
	
	public static Dao getInstance() {
		return dao;
	}
	
/* CREATE SCOPE */
	public <T> int insert(T type, Connection connection) throws Exception {
		return insert(null, type, null, 0, connection);
	}
	
	public <T> int insert(T type, String seqName, Connection connection) throws Exception {
		return insert(null, type, seqName, 0, connection);
	}
	
	public <T> int insert(T type, int depth, Connection connection) throws Exception {
		return insert(null, type, null, depth, connection);
	}
	
	public <T> int insert(T type, int depth, String seqName, Connection connection) throws Exception {
		return insert(null, type, seqName, depth, connection);
	}
	
	public <T> int insert(String tableName, T type, Connection connection) throws Exception {
		return insert(tableName, type, null, 0, connection);
	}
	
	public <T> int insert(String tableName, T type, String seqName, Connection connection) throws Exception {
		return insert(tableName, type, seqName, 0, connection);
	}
	
	public <T> int insert(T[] type, Connection connection) throws Exception {
		return insert(null, type, null, 0, connection);
	}
	
	public <T> int insert(T[] type, String seqName, Connection connection) throws Exception {
		return insert(null, type, seqName, 0, connection);
	}
	
	public <T> int insert(T[] type, int depth, Connection connection) throws Exception {
		return insert(null, type, null, depth, connection);
	}
	
	public <T> int insert(T[] type, int depth, String seqName, Connection connection) throws Exception {
		return insert(null, type, seqName, depth, connection);
	}
	
	public <T> int insert(String tableName, T[] type, Connection connection) throws Exception {
		return insert(tableName, type, null, 0, connection);
	}
	
	public <T> int insert(String tableName, T[] type, String seqName, Connection connection) throws Exception {
		return insert(tableName, type, seqName, 0, connection);
	}
	
	public <T> int insert(String tableName, T type, String seqName, int depth, Connection connection) throws Exception {
		Class<?> classe = null;
		Field[] notIgnoreFields = null, attachedFields = null;
		Method[] methods = null;
		String prepSql = null, insertedId = null;
		String[] seqInfo = null;
		PreparedStatement prepStmt = null;
		boolean connOpened = false;
		int result = 0;
		try {
			if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
			classe = type.getClass();
			tableName = getTableName(tableName, classe);
			notIgnoreFields = getFieldsByAnnot(classe, Ignore.class, false); // Not ignored fields
			prepSql = getInsertPrepSql(tableName, seqName, classe, notIgnoreFields);
			System.out.println(prepSql);
			prepStmt = connection.prepareStatement(prepSql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
			methods = getFieldsGetters(classe, notIgnoreFields);
			setInsertParameters(classe, prepStmt, type, methods, notIgnoreFields);
			result = prepStmt.executeUpdate();
			if(depth > 0) {
				attachedFields = getFieldsByAnnot(classe, Attached.class, true);
				seqInfo = getSeqInfo(type, notIgnoreFields);
				insertedId = getSequence(seqInfo[0], Dao.CURRVAL, connection);
				result += insertAttachedField(attachedFields, seqInfo[1]+insertedId, type, tableName, depth, connection);
			}
			if(connOpened) connection.commit();
			return result;
		} catch(Exception e) {
			if(connOpened) connection.rollback();
			throw e;
		} finally {
			close(prepStmt);
			close(connection, connOpened);
		}
	}
	
	public <T> int insert(String tableName, T[] types, String seqName, int depth, Connection connection) throws Exception {
		Class<?> classe = null;
		Field[] notIgnoredFields = null, attachedFields = null;
		Method[] methods = null;
		String prepSql = null, insertedId = null;
		String[] seqInfo = null;
		PreparedStatement prepStmt = null;
		boolean connOpened = false;
		int result = 0;
		try {
			if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
			classe = types[0].getClass();
			notIgnoredFields = getFieldsByAnnot(classe, Ignore.class, false); // Not ignored fields
			prepSql = getInsertPrepSql(tableName, seqName, classe, notIgnoredFields);
			prepStmt = connection.prepareStatement(prepSql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
			methods = getFieldsGetters(classe, notIgnoredFields);
			seqInfo = getSeqInfo(types[0], notIgnoredFields);
			for(T elem : types) {
				setInsertParameters(classe, prepStmt, elem, methods, notIgnoredFields);
				result += prepStmt.executeUpdate();
				if(depth > 0) {
					attachedFields = getFieldsByAnnot(classe, Attached.class, true);
					insertedId = getSequence(seqInfo[0], Dao.CURRVAL, connection);
					result += insertAttachedField(attachedFields, seqInfo[1]+insertedId, elem, tableName, depth, connection);
				}
			}
			if(connOpened) connection.commit(); 
			return result;
		}catch(Exception e) {
			if(connOpened) connection.rollback();
			throw e;
		} finally {
			close(prepStmt);
			close(connection, connOpened);
		}
	}
	
	private <T> void setInsertParameters(Class<?> c, PreparedStatement prepStmt, T type, Method[] methods, Field[] fields) throws Exception{
		int index = 1;
		for(int i = 0; i < fields.length; i++) {
			if(fields[i].isAnnotationPresent(Id.class))
				continue;
			setStmtParams(prepStmt, fields[i].getType().getSimpleName().toLowerCase(), methods[i], index, type);
			index++;
		}
	}
	
	private <T> String getInsertPrepSql(String tableName, String seqName, Class<?> classe, Field[] fields) throws Exception {
		tableName = getTableName(tableName, classe);
		String sql = null, prefix = "";
		sql = "INSERT INTO " + tableName + " (";
		for(int i = 0; i < fields.length; i++)
			sql += getFieldCol(fields[i]) + ",";
//		remove comma at the end
		sql = sql.substring(0, sql.length() - 1);
		sql += ") VALUES (";
		for(int i = 0; i < fields.length; i++) {
			if(fields[i].isAnnotationPresent(Id.class)) {
				if(fields[i].isAnnotationPresent(Seq.class)) {
					prefix = fields[i].getAnnotation(Seq.class).prefix();
					seqName = fields[i].getAnnotation(Seq.class).name();
				}
				if(getDbName().compareTo("oracle") == 0) {
					sql += "CONCAT('" + prefix + "'," + seqName + ".NEXTVAL),";
				} else {
					sql += "CONCAT('" + prefix + "'," + "NEXTVAL('"+ seqName + "')),";
				}
			} else {
				sql += "?,";				
			}
		}
		sql = sql.substring(0, sql.length() - 1);
		sql += ")";
		return sql;
	}
	
	private <T> Field[] getFieldsByAnnot(Class<?> classe, Class<? extends Annotation> annotClass, boolean target) {
		Field[] fields = getDeclaredFields(classe);
		List<Field> result = new ArrayList<Field>();
		for(Field f : fields) {
        	if((f.isAnnotationPresent(annotClass) && target) || !(f.isAnnotationPresent(annotClass) || target)) {
    			result.add(f);	
        	}
        	/*
        	 * if(Annotation present) : target == true
        	 * 	add
        	 * ---------------
        	 * if(Annotation not present) : target == false
        	 * 	add
        	 */
	    }
		return result.toArray(new Field[0]);
	}
	
	/**
	 * Recupère les getters des attributs <strong>fields</strong>
	 * @param c
	 * @param fields
	 * @return
	 * @throws Exception
	 */
	private Method[] getFieldsGetters(Class<?> classe, Field[] fields) throws Exception {
		List<Method> methods = new ArrayList<Method>();
		for(int i = 0; i < fields.length; i++)
			methods.add(classe.getMethod("get" + Utils.upperFirst(fields[i].getName())));
		return methods.toArray(new Method[0]);
	}
	
	private <T> String[] getSeqInfo(T type, Field[] fields) throws Exception {
		String[] result = new String[2];
		for(Field f: fields) {
    		if(f.isAnnotationPresent(Id.class) && f.isAnnotationPresent(Seq.class)) {
    			result[0] = f.getAnnotation(Seq.class).name();
    			result[1] = f.getAnnotation(Seq.class).prefix();
    			return result;
    		}
    	}
    	throw new Exception("Id or Seq annotation not set in class > "+type.getClass().getSimpleName());
	}
	
	@SuppressWarnings("unchecked")
	private <T> int insertAttachedField(Field[] fields, String id, T type, String tableName, int depth, Connection connection) throws Exception {
		Class<T> classe = null;
		Class<?> classeField = null;
		Method getter = null;
		Object[] data = null;
		Field fkField = null;
		int result = 0;
		try {
			classe = (Class<T>) type.getClass();
			for(Field f : fields) {
				getter = classe.getMethod("get"+Utils.upperFirst(f.getName()));
				if(f.getType().isArray()) {
					data = (Object[]) getter.invoke(type);
				} else {
					data = new Object[] {getter.invoke(type)};
				}
				classeField = data[0].getClass(); 
				fkField = getFkField(classeField, tableName);
				for(Object o : data) {
					classeField.getMethod("set"+ Utils.upperFirst(fkField.getName()), fkField.getType()).invoke(o, id);
				}
				result += insert(data, depth-1, connection);
			}
			return result;
		} catch (Exception e) {
			throw e;
		}
	}
	
/* READ SCOPE */
	public <T> T[] select(T type, Connection connection) throws Exception {
		return select(null, type, null, null, null, 0, 0, 0, 0, connection);
    }
	
	public <T> T[] select(String tableName, T type, Connection connection) throws Exception {
		return select(tableName, type, null, null, null, 0, 0, 0, 0, connection);
    }
	
	public <T> T[] select(T type, int depth, Connection connection) throws Exception {
		return select(null, type, null, null, null, 0, 0, depth, 0, connection);
    }
	
	public <T> T[] select(T type, int pageNb, int pageSize, Connection connection) throws Exception {
		return select(null, type, null, null, null, 0, 0, 0, 0, connection);
    }
	
	public <T> T[] select(T type, SqlMaker filtre, Connection connection) throws Exception {
		return select(null, type, null, filtre, null, 0, 0, 0, 0, connection);
    }
	
	public <T> T[] select(String tableName, T type, SqlMaker filtre, Connection connection) throws Exception {
		return select(tableName, type, null, filtre, null, 0, 0, 0, 0, connection);
    }
	
	@SuppressWarnings("unchecked")
	public <T> T[] select(T type, String sql, Connection connection) throws Exception {
    	Class<T> classe = null;
    	List<T> resultList = null;
    	Field[] fields = null;
    	T[] result = null;
    	PreparedStatement prepStmt = null;
    	ResultSet resultSet = null;
    	ResultSetMetaData rsMetaData = null;
    	boolean connOpened = false;
        try {
        	if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
        	classe = (Class<T>) type.getClass();
            prepStmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
            resultSet = prepStmt.executeQuery();
            rsMetaData = resultSet.getMetaData();
            resultList = new ArrayList<T>();
//          get fields and methods
         	fields = getDeclaredFields(classe);
//          retrieve result
            while (resultSet.next()) {
                resultList.add(resultSetToObject(resultSet, rsMetaData, classe, fields));
            }
            if (resultList.size() == 0) return null;
            result = resultList.toArray((T[])Array.newInstance(classe, 0));
            return result;
		} catch (Exception e) {
			throw e;
		} finally {
			close(resultSet, prepStmt);
			close(connection, connOpened);
		}
    }
	
	/**
	 * Récupère les données dans la <strong>table</strong>.Si l'argument <strong>table</strong> n'est pas spécifier, l'annotation de la table sera utilisé.Si <strong>@table</strong> n'est pas spécifier, le nom de la classe sera utilisé
	 * @param <strong>table</strong>
	 * Nom de la table dans la base
	 * @param <strong>type</strong>
	 * Type à retourner
	 * @param <strong>columns</strong>
	 * Nom des colonnes à recupérer
	 * @param <strong>where</strong>
	 * Filtre de recherche
	 * @param <strong>order</strong>
	 * Triage et regroupement des resultats
	 * @param <strong>pageNb</strong>
	 * Numéro de page
	 * @param <strong>pageSize</strong>
	 * Nombre d'element par page
	 * @param <strong>depth</strong>
	 * Profondeur de recherche(association)
	 * @param <strong>cache</strong>
	 * Spécifie si les données seront récuperés dans la base ou dans la cache et aussi au cas où il faut changer les données dedans
	 * @param <strong>connection</strong>
	 * @return <T>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T> T[] select(String tableName,T type, String columns, SqlMaker filtre, String afterWhere, int pageNb, int pageSize, int depth, int cacheConf, Connection connection) throws Exception {
    	Class<T> classe = null;
    	List<T> resultList = null;
    	Field[] fields = null, attachedFields = null;
    	T[] result = null;
    	PreparedStatement prepStmt = null;
    	ResultSet resultSet = null;
    	ResultSetMetaData rsMetaData = null;
    	Method getId = null;
    	String prepSql = null;
    	boolean connOpened = false;
        try {
        	if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
        	classe = (Class<T>) type.getClass();
//         	check table name
            tableName = getTableName(tableName, classe);
//         	get prepared sql
            prepSql = getSelectPrepSql(tableName, columns, filtre, afterWhere, pageNb, pageSize);
            prepStmt = connection.prepareStatement(prepSql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
//         	set sql parameters
            setSelectParameters(prepStmt, filtre, pageNb, pageSize);
            resultSet = prepStmt.executeQuery();
            rsMetaData = resultSet.getMetaData();
            resultList = new ArrayList<T>();
//          get fields and methods
         	fields = getDeclaredFields(classe);
         	attachedFields = getAttachedField(fields);
         	getId = getIdGetter(type, fields);
//          retrieve result
            while (resultSet.next()) {
                resultList.add(resultSetToObject(resultSet, rsMetaData, classe, fields));
                if(depth > 0) {
                	populateAttachedField(resultList.get(resultList.size() - 1), getId, attachedFields, cacheConf, depth-1, connection);
                }
            }
            if (resultList.size() == 0) return null;
            result = resultList.toArray((T[])Array.newInstance(classe, 0));
            if(cacheConf == Dao.SET_CACHE) {
            	Cache.set(classe, result);
            }
            return result;
		} catch (Exception e) {
			throw e;
		} finally {
			close(resultSet, prepStmt);
			close(connection, connOpened);
		}
    }
	
    private <T> String getTableName(String tableName, Class<T> classe) {
    	if(tableName == null || tableName.isEmpty()) {
        	tableName = classe.isAnnotationPresent(Table.class) ? classe.getAnnotation(Table.class).value() : classe.getSimpleName();
        }
    	return tableName;
    }
    
    private <T> Method getIdGetter(T type, Field[] fields) throws Exception {
    	for(Field f: fields) {
    		if(f.isAnnotationPresent(Id.class)) {
    			return type.getClass().getMethod("get"+Utils.upperFirst(f.getName()));
    		}
    	}
    	throw new Exception("Id annotation not set in class > "+type.getClass().getSimpleName());
    }
    
    private void setSelectParameters(PreparedStatement prepStmt, SqlMaker filtre, int pageNb, int pageSize) throws Exception {
    	if(filtre == null) {
    		if(pageNb != 0 && pageSize != 0) {
    			setPageParameters(prepStmt, pageNb, pageSize, 0);
    		}
    		return;
    	}
    	String[] params = filtre.getParamValues();
    	String[] types = filtre.getArrTypes();
    	int paramsNb = params.length; 
    	for(int i=0;i<paramsNb;i++) {
    		setStmtParams(prepStmt, types[i], params[i], i+1);
    	}
    	if(pageNb == 0 || pageSize == 0) {
    		return;
    	}
    	setPageParameters(prepStmt, pageNb, pageSize, paramsNb);
    }
    
    private void setPageParameters(PreparedStatement prepStmt, int pageNb, int pageSize, int indexParam) throws SQLException {
    	if(getDbName().compareTo("oracle") == 0) {
	    	prepStmt.setInt(indexParam + 1, (pageNb * pageSize + 1));
	    	prepStmt.setInt(indexParam + 2, ((pageNb-1) * pageSize + 1));
    	} else {
    		prepStmt.setInt(indexParam + 1, pageSize);
	    	prepStmt.setInt(indexParam + 2, (pageNb-1) * pageSize);
    	}
    }
    
    private String getSelectPrepSql(String table, String columns, SqlMaker filtre, String afterWhere, int pageNb, int pageSize) {
    	try {
    		String sql = null;
    		sql = (columns == null || columns.isEmpty()) ? "select *" : "select "+columns;
			sql += " from "+table;
			sql += (filtre == null || (filtre != null && filtre.emptySql())) ? "" : " where "+filtre.getPreparedSql();
			sql += (afterWhere == null || afterWhere.isEmpty()) ? "" : " "+afterWhere;
			if(pageNb == 0 || pageSize == 0) {
				return sql;    				
			}
			if(getDbName().compareTo("oracle") == 0) {
				sql = "SELECT * FROM ( SELECT result.*, rownum rn FROM ("+ sql +") result WHERE rownum < ? ) WHERE rn >= ?";				
			} else {
				sql += " limit ? offset ?";
			}
			return sql;
		} catch (Exception e) {
			throw e;
		}
    }
    
    private <T> Field[] getDeclaredFields(Class<?> classe) {
    	try {
			List<Field> fields = new ArrayList<Field>();
			Class<?> superClass = null;
			Collections.addAll(fields, classe.getDeclaredFields());
			superClass = classe.getSuperclass();
			if(superClass == null) return fields.toArray(new Field[0]);
			Collections.addAll(fields, getDeclaredFields(superClass));
			return fields.toArray(new Field[0]);
		} catch (Exception e) {
			throw e;
		}
    }
    
    private Field[] getAttachedField(Field[] fields) {
    	List<Field> result = new ArrayList<>();
    	for(Field f : fields) {
    		if(f.isAnnotationPresent(Attached.class)) {
    			result.add(f);
    		}
    	}
    	return result.size() == 0 ? null : result.toArray(new Field[0]);
    }
    
    private <T> T resultSetToObject(ResultSet resultSet, ResultSetMetaData rsMetaData, Class<T> classe, Field[] fields) throws Exception {
		T result = (T)classe.newInstance();
        Method method = null;
        Field field = null;
        for (int i = 0; i < rsMetaData.getColumnCount(); i++) {
        	if(rsMetaData.getColumnName(i + 1).compareToIgnoreCase("rn") == 0) {
        		continue;
        	}
        	field = getField(rsMetaData.getColumnName(i + 1), fields);
            if(field == null) {
        		// throw new Exception("Field not found for column > "+rsMetaData.getColumnName(i + 1));
            } else {
	            method = getSetter(field, classe);
	            invoke(method, result, field.getType().getSimpleName().toLowerCase(), resultSet, i+1);
            }
        }
        return result;
    }
    
    private Field getField(String colName, Field[] fields) {
    	String fieldName = null;
        for (int j = 0; j < fields.length; j++) {
        	fieldName = getFieldCol(fields[j]);
            if (colName.compareToIgnoreCase(fieldName) == 0) {
            	return fields[j];
        	}
        }
        return null;
    }
    
    private <T> Method getSetter(Field field, Class<T> classe) throws Exception {
    	return classe.getMethod("set"+Utils.upperFirst(field.getName()), field.getType());
    }
    
    private <T> void invoke(Method method, T instance, String argType, ResultSet rs, int colIndex) throws Exception {
		if (argType.compareTo("int") == 0 || argType.compareTo("class java.lang.int") == 0) {
        	method.invoke(instance, rs.getInt(colIndex));
        } else if (argType.compareTo("float") == 0 || argType.compareTo("class java.lang.float") == 0) {
        	method.invoke(instance, rs.getFloat(colIndex));
        } else if (argType.compareTo("double") == 0 || argType.compareTo("class java.lang.double") == 0) {
        	method.invoke(instance, rs.getDouble(colIndex));
        } else if (argType.compareTo("string") == 0 || argType.compareTo("class java.lang.String") == 0) {
        	method.invoke(instance, rs.getString(colIndex));
        } else if (argType.compareTo("date") == 0 || argType.compareTo("class java.sql.Date") == 0) {
        	method.invoke(instance, rs.getDate(colIndex));
        } else if (argType.compareToIgnoreCase("timestamp") == 0 || argType.compareTo("class java.sql.Timestamp") == 0) {
        	method.invoke(instance, rs.getTimestamp(colIndex));
        } else {
            throw new Exception("Argument type error > "+argType);
        }
    }
    
    /**
     * Si la classe est liée par un ou plusieurs table, on récupère les informations tant qu'il n'y ait plus un attribut <strong>attached</strong> ou <strong>depth</strong> = 0
     * @param <T>
     * @param type
     * @param getId
     * @param attachedFields
     * @param cacheConf
     * @param depth
     * @param c
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws Exception
     */
    private <T> void populateAttachedField(T type, Method getId, Field[] attachedFields, int cacheConf, int depth, Connection connection) throws InstantiationException, IllegalAccessException, Exception {
    	if(attachedFields == null) {
    		return;
    	}
    	SqlMaker filtre = null;
    	Method attachedMethod = null;
    	String className = null, id = null;
    	Object data = null;
    	Class<?> classe = null;
    	className = type.getClass().getSimpleName();
    	id = (String)getId.invoke(type);
    	for(Field f : attachedFields) {
    		if(f.getType().isArray()) {
    			classe = f.getType().getComponentType();
    			filtre = new SqlMaker(getFkColName(classe, className)).like(id);
	    		attachedMethod = type.getClass().getMethod("set"+Utils.upperFirst(f.getName()), f.getType());
	    		data = select(null, classe.newInstance(), null, filtre, null, 0, 0, depth, cacheConf, connection);
    		} else {
	    		classe = f.getType();
	    		filtre = new SqlMaker(getFkColName(classe, className)).like(id);
	    		attachedMethod = type.getClass().getMethod("set"+Utils.upperFirst(f.getName()), f.getType());
	    		data = select(null, classe.newInstance(), null, filtre, null, 0, 0, depth, cacheConf, connection);
	    		if(data != null) {
	    			data = ((Object[])data)[0];
	    		}
    		}
    		attachedMethod.invoke(type, data);
    	}
    }
    
    /**
     * Récupère les <strong>FK</strong> dans la classe <strong>type</strong> qui correspond à la table actuelle
     * @param <T>
     * @param type
     * @param tableName
     * @return
     * @throws Exception
     */
    private <T> String getFkColName(Class<?> type, String tableName) throws Exception {
    	Field[] fields = getDeclaredFields(type);
    	for(Field field : fields) {
    		if(field.isAnnotationPresent(Fk.class) && field.getAnnotation(Fk.class).value().compareToIgnoreCase(tableName) == 0) {
    			return getFieldCol(field);
    		}
    	}
    	throw new Exception("No attribute with FK annotation set in class > "+type.getClass().getSimpleName());
    }
    
    private <T> Field getFkField(Class<?> type, String tableName) throws Exception {
    	Field[] fields = getDeclaredFields(type);
    	for(Field f : fields) {
    		if(f.isAnnotationPresent(Fk.class) && f.getAnnotation(Fk.class).value().compareToIgnoreCase(tableName) == 0) {
    			return f;
    		}
    	}
    	throw new Exception("No attribute with FK annotation set in class > "+type.getClass().getSimpleName());
    }
    
/* UPDATE SCOPE */
    public <T> int update(T type, String[] cols, int ignore, Connection connection) throws Exception {
    	return update(null, type, cols, ignore, null, connection);
    }
    
    public <T> int update(T type, String[] cols, int ignore, SqlMaker filtre, Connection connection) throws Exception {
    	return update(null, type, cols, ignore, filtre, connection);
    }
    
    public <T> int update(String tableName, T type, String[] cols, int ignore, Connection connection) throws Exception {
    	return update(tableName, type, cols, ignore, null, connection);
    }
    
	@SuppressWarnings("unchecked")
	public <T> int update(String tableName, T type, String[] cols, int ignore, SqlMaker filtre, Connection connection) throws Exception {
		PreparedStatement prepStmt = null;
		Class<T> classe = null;
		String sql = "";
		Field[] fields = null;
		int result = 0;
		boolean connOpened = false;
        try {
        	if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
			classe = (Class<T>) type.getClass();
		// check table name
			tableName = getTableName(tableName, classe);
			if (cols == null || cols.length == 0) {
	            throw new Exception("No column to update !!!");
	        }
			fields = getFieldsToUpdate(classe, cols, ignore);
			sql = getPreparedUpdate(tableName, type, fields, ignore, filtre);
			prepStmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
			setUpdateParams(prepStmt, type, fields, filtre);
	        result = prepStmt.executeUpdate();
	        if(connOpened) connection.commit();
            return result;
        } catch (Exception e) {
        	if(connOpened) connection.rollback();
            throw e;
        } finally {
            close(prepStmt);
            close(connection, connOpened);
        }
    }
	
	private <T> void setUpdateParams(PreparedStatement prepStmt, T type, Field[] fields, SqlMaker filtre) throws Exception {
		String[] paramsValues = null;
		String[] paramsTypes = null;
		int nbCond = 0, nbMethod = 0, index = 0;
		String fieldType = "";
		Method getter = null;
		try {
			nbMethod = fields.length;
			if(filtre != null) {
				paramsValues = filtre.getParamValues();
				paramsTypes = filtre.getArrTypes();
				nbCond = paramsValues.length;
			}
	    	for(int i=0;i<nbMethod;i++) {
	    		getter = type.getClass().getMethod("get"+Utils.upperFirst(fields[i].getName()));
	    		fieldType = fields[i].getType().getSimpleName().toLowerCase();
	    		setStmtParams(prepStmt, fieldType, getter, i+1, type);
	    	}
	    	nbCond += nbMethod;
	    	for(int i=nbMethod;i<nbCond;i++) {
	    		index = i - nbMethod;
	    		setStmtParams(prepStmt, paramsTypes[index], paramsValues[index], i+1);
	    	}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private <T> Field[] getFieldsToUpdate(Class<T> classe, String[] cols, int target) throws Exception {
		Field[] fields = null;
		List<Field> result = new ArrayList<Field>();
		fields = getDeclaredFields(classe);
		boolean in = false;
		String colName = "";
		for(Field field : fields) {
			colName = getFieldCol(field);
			in = false;
			for(String col : cols) {
				if(colName.compareTo(col) == 0) {
					in = true;
					break;
				}
			}
			if((in && target == Dao.TO_UPDATE) || (!in && target == Dao.IGNORE)) {
				if(field.isAnnotationPresent(Ignore.class)) continue; // type of field not primitif
				result.add(field);
			}
		}
		if(result.size() == 0) {
			throw new Exception("Column name not found or no column to update !!!");
		}
		return result.toArray(new Field[0]);
	}
	
	private <T> String getPreparedUpdate(String table, T type, Field[] fields, int ignore, SqlMaker filtre) throws Exception {
		String prepSql = "";
        try {
	        prepSql = "update "+table+" set ";
	        for (int i = 0; i < fields.length; i++) {
        		prepSql += getFieldCol(fields[i]);
        		prepSql += " = ? ,";
	        }
	        prepSql = prepSql.substring(0, prepSql.length() - 1);
	        prepSql += (filtre == null) ? "" : " where "+filtre.getPreparedSql();  
	        return prepSql;
        } catch (Exception e) {
        	throw e;
		}
    }
	
/* DELETE SCOPE */
	public <T> int delete(T type, Connection connection) throws Exception {
		return delete(null, type, null, connection);
	}
	
	public int delete(String tableName, Connection connection) throws Exception {
		return delete(tableName, null, null, connection);
	}
	
	public int delete(String tableName, SqlMaker filtre, Connection connection) throws Exception {
		return delete(tableName, null, filtre, connection);
	}
	
	public <T> int delete(T type, SqlMaker filtre, Connection connection) throws Exception {
		return delete(null, type, filtre, connection);
	}
	
	@SuppressWarnings("unchecked")
	public <T> int delete(String tableName, T type, SqlMaker filtre, Connection connection) throws Exception {
		PreparedStatement prepStmt = null;
		Class<T> classe = null;
		String sql = "";
		int result = 0;
		boolean connOpened = false;
        try {
        	if(connection == null) {
				connection = Db.connect();
				connOpened = true;
			}
			if(type != null) classe = (Class<T>) type.getClass();
		// check class anotation
			tableName = getTableName(tableName, classe);    		
			sql = getPreparedDelete(tableName, filtre);
			prepStmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.CLOSE_CURSORS_AT_COMMIT);
			setDeleteParams(prepStmt, filtre);
	        result = prepStmt.executeUpdate();
	        if(connOpened) connection.commit();
            return result;
        } catch (Exception e) {
        	if(connOpened) connection.rollback();
            throw e;
        } finally {
            close(prepStmt);
            close(connection, connOpened);
        }
    }
	
	private <T> void setDeleteParams(PreparedStatement prepStmt, SqlMaker filtre) throws Exception {
		String[] paramValues = null;
		String[] paramTypes = null;
		int nbCond = 0;
		try {
			if(filtre == null) return;
			paramValues = filtre.getParamValues();
			paramTypes = filtre.getArrTypes();
			nbCond = paramValues.length;
	    	for(int i=0;i<nbCond;i++) {
	    		setStmtParams(prepStmt, paramTypes[i], paramValues[i], i+1);
	    	}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private <T> String getPreparedDelete(String table, SqlMaker filtre) throws Exception {
        try {
        	String sqlDelete = "";
        	sqlDelete = "delete from "+table;
	        sqlDelete += (filtre == null) ? "" : " where "+filtre.getPreparedSql();  
	        return sqlDelete;
        } catch (Exception e) {
        	throw e;
		}
    }
	
/* GLOBAL SCOPE */
	/**
	 * Ferme la connexion is elle est encore ouverte
	 * @param connection
	 * @throws SQLException
	 */
	private void close(Connection connection) throws SQLException {
		if(connection != null && !connection.isClosed()) connection.close();
	}
	
	private void close(Connection connection, boolean connOpened) throws SQLException {
		if(connOpened) {
			close(connection);
		}
	}
	
	/**
	 * Ferme le statement
	 * @param stmt
	 * @throws SQLException
	 */
	private void close(Statement stmt) throws SQLException {
		if(stmt != null && !stmt.isClosed()) stmt.close(); 
	}
	
	/**
	 * Ferme le resultset et le statement
	 * @param rs
	 * @param st
	 * @throws SQLException
	 */
	private void close(ResultSet rs, Statement st) throws SQLException {
		if(rs != null && !rs.isClosed()) rs.close();
    	if(st != null && !st.isClosed()) st.close();
    }
	
	private String getDbName() {
		return Db.prop.getProperty("DB_NAME");
	}
	
	private String getFieldCol(Field field) {
		return field.isAnnotationPresent(Column.class) ? field.getAnnotation(Column.class).value() : field.getName();
	}
	
	private <T> void setStmtParams(PreparedStatement prepStmt, String fieldType, Method getter, int index, T type) throws Exception {
		if (fieldType.compareTo("int") == 0 || fieldType.compareToIgnoreCase("class java.lang.int") == 0) {
        	prepStmt.setInt(index, (int) getter.invoke(type));
        } else if (fieldType.compareTo("float") == 0 || fieldType.compareToIgnoreCase("class java.lang.float") == 0) {
        	prepStmt.setFloat(index, (float) getter.invoke(type));
        } else if (fieldType.compareTo("double") == 0 || fieldType.compareToIgnoreCase("class java.lang.double") == 0) {
        	prepStmt.setDouble(index, (double) getter.invoke(type));
        } else if (fieldType.compareTo("string") == 0 || fieldType.compareToIgnoreCase("class java.lang.String") == 0) {
        	prepStmt.setString(index, (String) getter.invoke(type));
        } else if (fieldType.compareTo("timestamp") == 0 || fieldType.compareToIgnoreCase("class java.sql.Timestamp") == 0) {
        	prepStmt.setTimestamp(index, (Timestamp) getter.invoke(type));
        } else if (fieldType.compareTo("long") == 0) {
        	prepStmt.setLong(index, (long) getter.invoke(type));
        } else {
            throw new Exception("Field type not supported > "+fieldType);
        }
	}
	
	private <T> void setStmtParams(PreparedStatement prepStmt, String fieldType, String paramValue, int index) throws Exception {
		if(fieldType.compareTo("string") == 0) {
			prepStmt.setString(index, paramValue);
		} else if(fieldType.compareTo("int") == 0) {
			prepStmt.setInt(index, Integer.valueOf(paramValue).intValue());
		} else if(fieldType.compareTo("long") == 0) {
			prepStmt.setLong(index, Long.valueOf(paramValue).longValue());
		} else if(fieldType.compareTo("double") == 0) {
			prepStmt.setDouble(index, Double.valueOf(paramValue).doubleValue());
		} else if(fieldType.compareTo("float") == 0) {
			prepStmt.setFloat(index, Float.valueOf(paramValue).floatValue());
		} else if(fieldType.compareTo("timestamp") == 0) {
			prepStmt.setTimestamp(index, Utils.getDate(paramValue));
		} else {
			throw new Exception("Parameter type not supported > "+fieldType);
		}
	}
	
	public String getSequence(String seqName, int target, Connection connection) throws Exception {
		String sql = "select ";
		String targ = "";
		if(target == Dao.CURRVAL) {
			targ = "CURRVAL";
		} else if (target == Dao.NEXTVAL) {
			targ = "NEXTVAL";
		} else {
			throw new Exception("Target must be either CURRVAL or NEXTVAL");
		}
		if(getDbName().compareTo("oracle") == 0) {
			sql += seqName + "." + targ + " as value from dual";
		} else {
			sql += targ + "(" + seqName + ")";
		}
		Sequence[] sequences = select(new Sequence(), sql, connection);
		if(sequences == null) throw new Exception("Sequence doesn't exist > " + seqName);
		return sequences[0].getValue();
	}

    public <T> Method[] getMethods(T type, String filtre) throws Exception {
    	try {
	        Class<?> cls = type.getClass();
	        List<Field> flds = new ArrayList<Field>();
			Collections.addAll(flds, getDeclaredFields(cls));
			Field[] fields = flds.toArray(new Field[0]);
	        int nbreAttr = fields.length;
	        Vector<Method> methods = new Vector<Method>();
	        String nF = null;
	        for (int i = 0; i < nbreAttr; i++) {
	            nF = filtre + Utils.upperFirst(fields[i].getName());
	            try {
	                if (filtre.compareTo("set") == 0) {
	                	methods.add(cls.getMethod(nF, fields[i].getType()));
	                } else if (filtre.compareTo("get") == 0) {
	                	methods.add(cls.getMethod(nF));
	                }
	            } catch (NoSuchMethodException e) {
	                throw new Exception("no such method " + nF + " in class " + cls.getSimpleName());
	            }
	        }
	        return methods.toArray(new Method[0]);
    	} catch (Exception e) {
			throw e;
		}
    }
    
    public <T> Method[] getNotNullMethods(T type) throws Exception {
		ArrayList<Method> result = null;
		Method[] methodes = null;
		Object returned = null;
		try {
			result = new ArrayList<Method>();
	        methodes = getMethods(type, "get");
	        for (int i = 0; i < methodes.length; i++) {
	        	returned = methodes[i].invoke(type); 
	            if (returned != null && !returned.equals("") && returned.toString().compareTo("0") != 0 && returned.toString().compareTo("0.0") != 0) {
	            	result.add(methodes[i]);
	            }
	        }
	        if (result.size() == 0) {
	            return null;
	        }
	        return result.toArray(new Method[0]);
		} catch (Exception e) {
			throw e;
		}
    }
}
	