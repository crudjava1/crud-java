package dao;

import java.util.HashMap;

import annotation.Cacheable;

/**
 * La classe Cache stocke les données que l'utilisateur veut mettre en cache.
 * @author Bary Rakotoarivelo
 * @author Marcus Hunald
 */
public class Cache {
	/**
	 * Stocke les données pour chaque classe a mettre en cache.<br>
	 * La clé <strong> Class </strong> pour identifier les données par classe.<br>
	 * La valeur <strong> Object </strong> pour stocker les données (liste d'objet) de chaque Classe  
	 */
	private static HashMap<Class<?>, Object> value = new HashMap<Class<?>, Object>();
	
	/**
	 * 
	 * @param <T> Le type d'objet a retourner
	 * @param classe Classe pour récuperer les données en cache
	 * @return
	 * Liste d'objets correspondant à la classe donnée en argument
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] get(Class<T> classe) {
		T[] data = null;
    	Cacheable cache = classe.getAnnotation(Cacheable.class);
		if(cache != null) {
			data = (T[]) value.get(classe);
		}
		return data;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] get(T type) {
		return get((Class<T>)type.getClass());
	}
	
	public static <T> void set(Class<T> classe, T[] data) {
		value.put(classe, data);
	}
}
