package dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Classe qui gère la connexion au SGBD
 * @author Bary Rakotoarivelo
 */
public class Db {
	public static Properties prop = null;
	static {
		try {
			prop = readPropertiesFile("dao.properties");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * connecte au SGBD correspondant aux données du fichier properties
	 * @return
	 * @throws Exception
	 */
	public static Connection connect() throws Exception {
		prop = readPropertiesFile("dao.properties");
		String dbName = prop.getProperty("DB_NAME");
		if(dbName.compareTo("postgres") == 0) {
			return pgConnect();
		}
		if(dbName.compareTo("oracle") == 0) {
			return oracleConnect();
		}
		throw new Exception("DB_NAME not found : "+dbName);
	}
	
	private static Connection oracleConnect() throws Exception {
		Class.forName(prop.getProperty("DRIVER_NAME"));
		try {
			Connection connection = null;
	        connection = DriverManager.getConnection(prop.getProperty("JDBC_DATABASE_URL"), prop.getProperty("USERNAME"), prop.getProperty("PASSWORD"));
	        connection.setAutoCommit(false);
	        return connection;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static Connection pgConnect() throws Exception {
		try {
			Properties props = new Properties();
			props.setProperty("user", prop.getProperty("USERNAME"));
			props.setProperty("password", prop.getProperty("PASSWORD"));
			//props.setProperty("ssl","true");
			Connection connection = DriverManager.getConnection(prop.getProperty("JDBC_DATABASE_URL"), props);
	        connection.setAutoCommit(false);
	        return connection;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static Properties readPropertiesFile(String fileName) throws Exception {
	      FileInputStream fis = null;
	      Properties prop = null;
	      try {
	         fis = new FileInputStream(fileName);
	         prop = new Properties();
	         prop.load(fis);
	      } catch(Exception e) {
	         e.printStackTrace();
	      } finally {
	         fis.close();
	      }
	      return prop;
	}
}
