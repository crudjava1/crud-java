package dao.crud;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import annotation.Attached;
import annotation.Column;
import annotation.Ignore;
import dao.DbConnect;

public class Create {
	/*public static <T> void create(Connection connection, T object) throws Exception {
		PreparedStatement pstmt = null;
		try {
            connection = DbConnect.connect();
			Class<?> c = object.getClass();
			Field[] fields = Create.getFields(c);
			String query = Create.setQuery(c, object, fields);
			pstmt = connection.prepareStatement(query);
			Method[] methods = Create.getMethods(c, fields);
			Create.setPreparedStatment(c, pstmt, object, methods, fields);
		}catch(Exception ex) {
			ex.printStackTrace();
			throw new Exception("Insert data into db failed:");
		}finally {
			Read.close(null, pstmt);
		}
	}
	
	public static <T> void create(T object) throws Exception {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
            connection = DbConnect.connect();
			//create(connection, object);
			create(connection, object, 0);
		}catch(Exception ex) {
			throw new Exception(ex.getMessage());
		}finally {
			Read.close(null, pstmt);
			DbConnect.close(connection);
		}
	}
	
	///insert depth
	public static <T> void create(Connection connection, T object, int depth) throws Exception {
		try {
			if(depth == 0) {
				create(connection, object);
				//Create.create(connection, object);
			}
			Class<?> c = object.getClass();
			Field[] fields = Create.getAllFields(c);
			for(int i = 0; i < fields.length; i++) {
				if(fields[i].isAnnotationPresent(Attached.class)) {
					Method meth = c.getMethod("get" + Create.toUpperCaseFirstLetter(fields[i].getName()));
					Object fk = meth.invoke(object);
					if(fk.getClass().isArray()) {
						fk = meth.getReturnType().cast(fk);
						///create function with fk array;
						create(connection, fk, depth-1);
					}else {
						//Create.create(connection, meth.invoke(object));
						create(connection, object, depth-1);
					}
				}
			}
		}catch(NoSuchMethodException noMethEx) {
			throw new Exception("Method get association not found");
		}
	}
	
	///insert array of object depth
	public static <T> void create(Connection connection, T[] fks, int depth) throws Exception {
		for(Object fk : fks){
			//Create.create(connection, fk);
			//set foreign key
			Create.create(connection, fk, depth);
		 }
	}
	
	public static <T> void create(T[] object) throws Exception {
		Connection connection = null;
		PreparedStatement pstmt = null;
		try {
            connection = DbConnect.connect();
			Class<?> c = object[0].getClass();
			Field[] fields = Create.getFields(c);
			String query = Create.setQuery(c, object[0], fields);
			pstmt = connection.prepareStatement(query);
			Method[] methods = Create.getMethods(c, fields);
			Create.setPreparedStatment(c, pstmt, object, methods, fields);
		}catch(Exception ex) {
			throw new Exception("Insert data into db failed:");
		}finally {
			Read.close(null, pstmt);
			DbConnect.close(connection);
		}
	}
	
	public static <T> void setPreparedStatment(Class<?> c, PreparedStatement pstmt, T object, Method[] methods, Field[] fields) throws Exception{
		int j = 1;
		for(int i = 0; i < fields.length; i++) {
				switch((fields[i].getType().getSimpleName()).toLowerCase()) {
					case "int":
						pstmt.setInt(j, (int) methods[i].invoke(object));
						break;
					case "string":
						pstmt.setString(j, (String) methods[i].invoke(object));
						break;
					case "double":
						pstmt.setDouble(j, (double) methods[i].invoke(object));
						break;
					case "float":
						pstmt.setFloat(j, (float) methods[i].invoke(object));
						break;
					case "timestamp":
						pstmt.setTimestamp(j, (Timestamp) methods[i].invoke(object));
						break;
				}
				j++;
		}
		pstmt.executeUpdate();
	}

	public static <T> void setPreparedStatment(Class<?> c, PreparedStatement pstmt, T[] object, Method[] methods, Field[] fields) throws Exception{
		for(int i = 0; i < object.length; i++)
			Create.setPreparedStatment(c, pstmt, object[i], methods, fields);
	}
	
	public static <T> String setQuery(Class<?> c, T object, Field[] fields) throws Exception{
		String query = "INSERT INTO " + c.getSimpleName() + " (";
		for(int i = 0; i < fields.length; i++)
			query += ((fields[i].isAnnotationPresent(Column.class))? fields[i].getAnnotation(Column.class).value() : fields[i].getName()) + ",";
		query = query.substring(0, query.length() - 1);
		query += ") VALUES(";
		for(int i = 0; i < fields.length; i++)
			query += "?,";
		query = query.substring(0, query.length() - 1);
		query += ")";
		return query;
	}
	
	public static <T> Field[] getFields(Class<?> c) {
		List<Field> fields = new ArrayList<Field>();
		while (c != null) {
	        for (Field field : c.getDeclaredFields()) {
	        	if(field.isAnnotationPresent(Ignore.class))
	        		continue;
            	fields.add(field);
	        }
	        c = c.getSuperclass();
	    }
		return fields.toArray(new Field[fields.size()]);
	}
	
	public static <T> Field[] getAllFields(Class<?> c) {
		List<Field> fields = new ArrayList<Field>();
		while (c != null) {
	        for (Field field : c.getDeclaredFields())
            	fields.add(field);
	        c = c.getSuperclass();
	    }
		return fields.toArray(new Field[fields.size()]);
	}
	
	public static Method[] getMethods(Class<?> c, Field[] fields) throws Exception {
		List<Method> methods = new ArrayList<Method>();
		for(int i = 0; i < fields.length; i++)
			methods.add(c.getMethod("get" + Create.toUpperCaseFirstLetter(fields[i].getName())));
		return methods.toArray(new Method[methods.size()]);
	}
	
	public static String toUpperCaseFirstLetter(String arg) {
		char[] chars = arg.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		return String.valueOf(chars);
	}
	
	public static String toLowerCaseFirstLetter(String arg) {
		char[] chars = arg.toCharArray();
		chars[0] = Character.toLowerCase(chars[0]);
		return String.valueOf(chars);
	}*/
}