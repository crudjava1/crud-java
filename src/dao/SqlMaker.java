package dao;

import java.util.ArrayList;
import java.util.List;

import utils.Utils;

/**
 * Gère les operateurs entre 2 ou plusieurs conditions de recherche (find)
 * @author Bary Rakotoarivelo
 */
public class SqlMaker {
	private String preparedSql = "";
	private List<String> values = new ArrayList<String>();
	private List<String> types = new ArrayList<String>();
	private boolean colName = false;
	
	public SqlMaker(String col) {
		preparedSql += col;
		colName = true;
	}
	
	public SqlMaker closeBrackets() {
		preparedSql = "(" + preparedSql + ")";
		return this;
	}
	
	public <T> SqlMaker like(T value) throws SqlMakerException {
		hasColName();
		String type = value.getClass().getSimpleName();
		preparedSql += Utils.isNumber(type) ? " = ?" : " like ?";
		add(value);
		return this;
	}
	
	public <T> SqlMaker lt(T value) throws SqlMakerException {
		hasColName();
		preparedSql += " < ?";
		add(value);
		return this;
	}
	
	public <T> SqlMaker gt(T value) throws SqlMakerException {
		hasColName();
		preparedSql += " > ?";
		add(value);
		return this;
	}
	
	public <T> SqlMaker between(T start, T end) throws SqlMakerException {
		hasColName();
		preparedSql += " between ? and ?";
		add(start, end);
		return this;
	}
	
	public <T> SqlMaker notBetween(T start, T end) throws SqlMakerException {
		hasColName();
		preparedSql += " not between ? and ?";
		add(start, end);
		return this;
	}
	
	public <T> SqlMaker and(SqlMaker filtre) {
		preparedSql += " and ("+filtre.getPreparedSql() + ")";
		add(filtre);
		return this;
	}
	
	public <T> SqlMaker and(String col) {
		preparedSql += " and "+col;
		colName = true;
		return this;
	}
	
	public <T> SqlMaker or(SqlMaker filtre) {
		preparedSql += " or ("+filtre.getPreparedSql() + ")";
		add(filtre);
		return this;
	}
	
	public <T> SqlMaker or(String col) {
		preparedSql += " or "+col;
		colName = true;
		return this;
	}
	
	public String[] getArrTypes() {
		return types.toArray(new String[0]);
	}
	
	public String[] getParamValues() {
		return values.toArray(new String[0]);
	}
	
	public boolean emptySql() {
		return preparedSql.isEmpty();
	}
	
	private void hasColName() throws SqlMakerException {
		if(!colName) {
			throw new SqlMakerException("Nom de colonne associée introuvable !");
		}
	}
	
	private void add(SqlMaker filtre) {
		values.addAll(filtre.getValues());
		types.addAll(filtre.getTypes());
	}
	
	private <T> void add(T value) {
		values.add(value.toString());
		types.add(value.getClass().getSimpleName().toLowerCase());
		colName = false;
	}
	
	private <T> void add(T start, T end) {
		add(start);
		add(end);
	}
	
	public String getPreparedSql() {
		return preparedSql;
	}
	public void setPreparedSql(String preparedSql) {
		this.preparedSql = preparedSql;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}

	public List<String> getTypes() {
		return types;
	}

	public void setTypes(List<String> types) {
		this.types = types;
	}
	
}
