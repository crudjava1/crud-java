package models;

import java.sql.Timestamp;
import java.util.Arrays;

import annotation.Column;
import annotation.Id;
import annotation.Ignore;
import annotation.Seq;
import annotation.Attached;
import annotation.Cacheable;
import annotation.Table;

@Cacheable()
@Table("etudiant")
public class Etudiant {
	
	@Id
	@Seq(name="s_etudiant", prefix="etu")
	@Column("id")
	private String id;
	
	@Column("nom")
	private String prenom;
	
	@Column("dateNaiss")
	private Timestamp dateNaiss;
	
	@Column("email")
	private String email;
	
	@Ignore
	@Attached
	private Diplome[] diplomes;
	
	public Etudiant() {}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String nom) {
		this.prenom = nom;
	}
	
	public Diplome[] getDiplomes() {
		return diplomes;
	}
	
	public void setDiplomes(Diplome[] diplomes) {
		this.diplomes = diplomes;
	}
	
	public void addDiplomes(Diplome diplome) {
		Diplome[] temp = this.diplomes;
		this.diplomes = new Diplome[this.diplomes.length + 1];
		this.diplomes = temp;
		this.diplomes[this.diplomes.length - 1] = diplome;
	}

	public Timestamp getDateNaiss() {
		return dateNaiss;
	}

	public void setDateNaiss(Timestamp dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		String result = "";
		result += "Etudiant [id=" + id + ", nom=" + prenom + ", dateNaiss=" + dateNaiss + ", email=" + email + ", diplomes=";
		if(diplomes != null) {
			for(Diplome d : diplomes) {
				result += "\n\t" + d.toString();
			}
			result += "\n]";
		} else {
			result += Arrays.toString(diplomes) + "]";
		}
		return result;
	}
	
	public Etudiant(String pernom, Timestamp dateNaiss, String email) {
		this.prenom = pernom;
		this.dateNaiss = dateNaiss;
		this.email = email;
	}
}
