package models;

import annotation.Column;
import annotation.Fk;
import annotation.Id;
import annotation.Seq;
import annotation.Table;

@Table("diplome")
public class Diplome {
	
	@Id
	@Seq(name="s_diplome", prefix="etu")
	@Column("id")
	private String id;
	
	@Column("des")
	private String des;
	
	@Column("idEtudiant")
	@Fk("etudiant")
	private String idEtudiant;
	
	public Diplome() {}
	
	public String getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return "Diplome [id=" + id + ", des=" + des + ", idEtudiant=" + idEtudiant + "]";
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getDes() {
		return des;
	}
	
	public void setDes(String des) {
		this.des = des;
	}

	public String getIdEtudiant() {
		return idEtudiant;
	}

	public void setIdEtudiant(String idEtudiant) {
		this.idEtudiant = idEtudiant;
	}

	public Diplome(String des) {
		this.des = des;
	}
}
