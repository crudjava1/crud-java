CREATE TABLE public.etudiant (
    id character varying(20) NOT NULL PRIMARY KEY,
    nom character varying(30),
	sexe char,
    datenaiss timestamp without time zone,
    email character varying(30)
);

create TABLE public.diplome (
    id character varying(20) NOT NULL PRIMARY KEY,
    idetu character varying(20),
    libelle character varying(30),
    foreign key (idetu) references etudiant(id)
);